package be.multimedi.pokemonDB.pokemonSpecies;

import be.multimedi.pokemonDB.pokemonType.PokemonType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.pokemonDB.db.DriverManagerWrapper.*;

public final class PokemonSpeciesDAO {
   private PokemonSpeciesDAO() {
   }

   public static List<PokemonSpecies> getAllPokemonSpecies() {
      List<PokemonSpecies> list = new ArrayList<>();
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonSpecies");) {
         while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int typeId1 = rs.getInt(3);
            int typeId2 = rs.getInt(4);
            int parentEvolutionId = rs.getInt(5);
            int officialPokemonId = rs.getInt(6);
            try {
               PokemonSpecies ps = new PokemonSpecies(id, name, typeId1, typeId2, parentEvolutionId, officialPokemonId);
               list.add(ps);
            } catch (IllegalArgumentException iae) {
               System.out.println("Could not read PokemonSpecies: Illegal data in DB! " + iae);
            }
         }//--> while(rs.next())
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return list;
   }

   public static boolean removePokemonSpeciesById(int speciesId) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("DELETE FROM PokemonSpecies WHERE id = ?");
      ) {
         stmt.setInt(1, speciesId);
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }

   public static String getSpeciesNameById(int speciesID){
       try (Connection con = DriverManager.getConnection(url, login, pwd);
                          Statement stmt = con.createStatement();
                          ResultSet rs = stmt.executeQuery(
                                  "SELECT name FROM PokemonSpecies WHERE id = " + speciesID)) {
           if (rs.next()) {
               return rs.getString(1);
           }
       } catch (SQLException se) {
           System.out.println("oops : " + se);
       } catch (IllegalArgumentException iae) {
           System.out.println("Illegal data in Db! " + iae);
       }
       return null;
   }

    public static boolean registerPokemonSpecies(PokemonSpecies ps) {
        //TODO
        return false;
    }
}
