package be.multimedi.pokemonDB.pokemonType;

import be.multimedi.pokemonDB.trainer.Trainer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static be.multimedi.pokemonDB.db.DriverManagerWrapper.*;

public final class PokemonTypeDAO {
   private PokemonTypeDAO() {
   }

   public static String getTypeStrById(int typeId) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery(
                   "SELECT type FROM PokemonType WHERE id = " + typeId)) {
         if (rs.next()) {
            return rs.getString(1);
         }
      } catch (SQLException se) {
         System.out.println("oops : " + se);
      } catch (IllegalArgumentException iae) {
         System.out.println("Illegal data in Db! " + iae);
      }
      return null;
   }

   public static List<PokemonType> getAllPokemonTypes() {
      List<PokemonType> list = new ArrayList<>();
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           Statement stmt = con.createStatement();
           ResultSet rs = stmt.executeQuery("SELECT * FROM PokemonType");) {
         while (rs.next()) {
            int id = rs.getInt(1);
            String type = rs.getString(2);
            try {
               PokemonType pt = new PokemonType(id, type);
               list.add(pt);
            } catch (IllegalArgumentException iae) {
               System.out.println("Could not read PokemonType: Illegal data in DB! " + iae);
            }
         }//--> while(rs.next())
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return list;
   }

   public static boolean registerPokemonType(PokemonType pt) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("INSERT INTO PokemonType (type) VALUES (?)");
      ) {
         stmt.setString(1, pt.getType());
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }

   public static boolean removePokemonTypeById(int TypeId) {
      try (Connection con = DriverManager.getConnection(url, login, pwd);
           PreparedStatement stmt = con.prepareStatement("DELETE FROM PokemonType WHERE id = ?");
      ) {
         stmt.setInt(1, TypeId);
         int changes = stmt.executeUpdate();
         if (changes > 0) {
            return true;
         }
      } catch (SQLException se) {
         System.out.println("oops: " + se);
      }
      return false;
   }

   public static boolean removePokemonType(PokemonType pt) {
      if (pt == null) return false;
      return removePokemonTypeById(pt.getId());
   }
}
