package be.multimedi.pokemonDB.pokemonType;

public class PokemonType {
   int id;
   String type;

   public PokemonType(int id, String type) {
      setId(id);
      setType(type);
   }

   public PokemonType(String type) {
      this(-1, type);
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getType() {
      return type;
   }

   public void setType(String type) {
      if (type == null || type.isBlank()) throw new IllegalArgumentException("type is required");
      this.type = type;
   }

   @Override
   public String toString() {
      return "PokemonType{" +
              "id=" + id +
              ", type='" + type + '\'' +
              '}';
   }
}
