package be.multimedi.pokemonDB.trainer;

import java.time.LocalDate;

public class Trainer {
   int id;
   String firstName;
   String lastName;
   LocalDate birthday;
   String city;

   public Trainer(int id, String firstName, String lastName, LocalDate birthday, String city) {
      setId(id);
      setFirstName(firstName);
      setLastName(lastName);
      setBirthday(birthday);
      setCity(city);
   }

   public Trainer(String firstName, String lastName, LocalDate birthday, String city) {
      this(-1, firstName, lastName, birthday, city);
   }

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      if (firstName == null || firstName.isBlank()) throw new IllegalArgumentException("firstName is required!");
      this.firstName = firstName;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      if (lastName == null || lastName.isBlank()) throw new IllegalArgumentException("lastName is required!");
      this.lastName = lastName;
   }

   public LocalDate getBirthday() {
      return birthday;
   }

   public void setBirthday(LocalDate birthday) {
      if (birthday == null) throw new IllegalArgumentException("birthday is required!");
      this.birthday = birthday;
   }

   public String getCity() {
      return city;
   }

   public void setCity(String city) {
      if (city == null || city.isBlank()) throw new IllegalArgumentException("city is required!");
      this.city = city;
   }

   @Override
   public String toString() {
      return "Trainer{" +
              "id=" + id +
              ", firstName='" + firstName + '\'' +
              ", lastName='" + lastName + '\'' +
              ", birthday=" + birthday +
              ", city='" + city + '\'' +
              '}';
   }
}
