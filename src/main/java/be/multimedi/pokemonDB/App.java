package be.multimedi.pokemonDB;

import be.multimedi.pokemonDB.consoleTools.ConsoleColors;
import be.multimedi.pokemonDB.db.DriverManagerWrapper;
import be.multimedi.pokemonDB.pokemonPets.PokemonPet;
import be.multimedi.pokemonDB.pokemonPets.PokemonPetDAO;
import be.multimedi.pokemonDB.pokemonSpecies.PokemonSpecies;
import be.multimedi.pokemonDB.pokemonSpecies.PokemonSpeciesDAO;
import be.multimedi.pokemonDB.pokemonType.PokemonType;
import be.multimedi.pokemonDB.pokemonType.PokemonTypeDAO;
import be.multimedi.pokemonDB.trainer.Trainer;
import be.multimedi.pokemonDB.trainer.TrainerDAO;

import java.time.LocalDate;
import java.util.List;

import static be.multimedi.pokemonDB.consoleTools.ConsoleInputTool.*;
import static be.multimedi.pokemonDB.consoleTools.ConsoleOutputTool.*;

/**
 * Hello pokemon world-App!
 */
public class App {
    private App() {
    }

    private final String APP_NAME = "Pokemon manager";

    public static void main(String[] args) {
        App app = new App();
        app.startApplication();
    }

    public void startApplication() {
        System.out.println("Database-url = " + DriverManagerWrapper.url);
        System.out.println("Welcome to the " + APP_NAME + "!");
        int choice = 0;
        do {
            System.out.println();
            printTitle("Main menu:");
            System.out.println("1. trainer menu");
            System.out.println("2. pokemon types menu");
            System.out.println("3. pokemon species menu");
            System.out.println("4. pokemon pets menu");
            System.out.println("0. exit " + APP_NAME);
            choice = askUserInteger("Your choice: ", 0, 4);
            switch (choice) {
                case 1:
                    trainerMenu();
                    break;
                case 2:
                    pokemonTypesMenu();
                    break;
                case 3:
                    pokemonSpeciesMenu();
                    break;
                case 4:
                    pokemonPetsMenu();
                    break;
            }
        } while (choice > 0);
        System.out.println("\nGood bye!");
    }

    private void trainerMenu() {
        int choice;
        do {
            System.out.println();
            printTitle("Pokemon trainer menu:");
            System.out.println("1. Overview trainers");
            System.out.println("2. Register new trainer");
            System.out.println("3. Details trainer menu");
            System.out.println("0. to Main Menu ");
            choice = askUserInteger("Your choice: ", 0, 4);
            switch (choice) {
                case 1:
                    overviewTrainers();
                    break;
                case 2:
                    registerNewTrainer();
                    break;
                case 3:
                    detailsTrainerMenu();
                    break;
            }
        } while (choice > 0);
    }

    private void pokemonTypesMenu() {
        int choice;
        do {
            System.out.println();
            printTitle("Pokemon types menu:");
            System.out.println("1. Overview types");
            System.out.println("2. Register new pokemon type");
            System.out.println("3. Details type menu");
            System.out.println("0. to Main Menu ");
            choice = askUserInteger("Your choice: ", 0, 4);
            switch (choice) {
                case 1:
                    overviewPokemonTypes();
                    break;
                case 2:
                    registerNewPokemonType();
                    break;
                case 3:
                    detailsPokemonTypeMenu();
                    break;
            }
        } while (choice > 0);
    }

    private void pokemonSpeciesMenu() {
        int choice;
        do {
            System.out.println();
            printTitle("Pokemon species menu:");
            System.out.println("1. Overview species");
            System.out.println("2. Register new pokemon species");
            System.out.println("3. Details species menu");
            System.out.println("0. to Main Menu ");
            choice = askUserInteger("Your choice: ", 0, 4);
            switch (choice) {
                case 1:
                    overviewPokemonSpecies();
                    break;
                case 2:
                    registerNewPokemonSpecies();
                    break;
                case 3:
                    detailsPokemonSpeciesMenu();
                    break;
            }
        } while (choice > 0);
    }

    private void pokemonPetsMenu() {
        int choice;
        do {
            System.out.println();
            printTitle("Pokemon pets menu:");
            System.out.println("1. Overview pets");
            System.out.println("2. Register new pokemon pets");
            System.out.println("3. Details pet menu");
            System.out.println("0. to Main Menu ");
            choice = askUserInteger("Your choice: ", 0, 4);
            switch (choice) {
                case 1:
                    overviewPokemonPets();
                    break;
                case 2:
                    registerNewPokemonPets();
                    break;
                case 3:
                    detailsPokemonPetMenu();
                    break;
            }
        } while (choice > 0);
    }

    private void overviewTrainers() {
        List<Trainer> list = TrainerDAO.getAllTrainers();
        if (list == null || list.size() == 0) {
            printAlert("No trainers in the database");
        } else {
            System.out.println();
            printTitle("Trainers");
            setPrintColor(ConsoleColors.SINGLE_UNDERLINE);
            System.out.printf("%3s | %12s | %12s | %10s | %12s\n",
                    "id", "firstname", "lastname", "birthday", "city");
            resetPrintColor();
            for (Trainer t : list) {
                System.out.printf("%3d | %12s | %12s | %10s | %12s\n",
                        t.getId(), t.getFirstName(), t.getLastName(), t.getBirthday(), t.getCity());
            }
        }
        askPressEnterToContinue();
    }

    void overviewPokemonTypes() {
        List<PokemonType> list = PokemonTypeDAO.getAllPokemonTypes();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon types in the database");
        } else {
            System.out.println();
            printTitle("Pokemon types");
            setPrintColor(ConsoleColors.SINGLE_UNDERLINE);
            System.out.printf("%3s | %12s\n", "id", "type");
            resetPrintColor();
            for (PokemonType pt : list) {
                System.out.printf("%3d | %12s\n", pt.getId(), pt.getType());
            }
        }
        askPressEnterToContinue();
    }

    void overviewPokemonSpecies() {
        List<PokemonSpecies> list = PokemonSpeciesDAO.getAllPokemonSpecies();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon species in the database");
        } else {
            List<PokemonType> typeList = PokemonTypeDAO.getAllPokemonTypes();
            System.out.println();
            printTitle("Pokemon types");
            setPrintColor(ConsoleColors.SINGLE_UNDERLINE);
            System.out.printf("%3s | %16s | %8s | %8s\n", "id", "name", "type1", "type2");
            resetPrintColor();
            for (PokemonSpecies ps : list) {
                String type1 = getPokemonType(typeList, ps.getTypeId1());
                String type2 = getPokemonType(typeList, ps.getTypeId2());
                if (type2 == null)
                    type2 = ConsoleColors.FG_BLUE.getColorStr() + "null" + ConsoleColors.RESET.getColorStr();
                System.out.printf("%3d | %16s | %8s | %8s\n", ps.getId(), ps.getName(), type1, type2);
            }
        }
        askPressEnterToContinue();
    }

    String getPokemonType(List<PokemonType> list, int typeId){
        for( PokemonType pt : list){
            if( pt.getId() == typeId)
                return pt.getType();
        }
        return null;
    }

    void overviewPokemonPets() {
        List<PokemonPet> list = PokemonPetDAO.getAllPokemonPets();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon species in the database");
        } else {
            System.out.println();
            printTitle("Pokemon types");
            setPrintColor(ConsoleColors.SINGLE_UNDERLINE);
            printTitle(String.format("%3s | %16s | %8s | %8s\n", "id", "name", "species", "trainer" ));
            resetPrintColor();
            for (PokemonPet pp : list) {
                String name = pp.getName();
                if (name == null)
                    name = ConsoleColors.FG_BLUE.getColorStr() + "null" + ConsoleColors.RESET.getColorStr();
                System.out.printf("%3d | %16s | %8d | %8d\n", pp.getId(), name, pp.getSpeciesID(), pp.getTrainerID());
            }
        }
        askPressEnterToContinue();
    }

    private void registerNewTrainer() {
        System.out.println();
        printTitle("Registering new pokemon trainers:");
        do {
            String firstName = askUserString("first name: ", 2);
            String lastName = askUserString("last name: ", 2);
            LocalDate birthday = askUserDateBefore("birthday: ", LocalDate.now().minusYears(12));
            String city = askUserString("city: ", 2);
            Trainer t = new Trainer(firstName, lastName, birthday, city);
            System.out.println(t);
            if (askUserYesNoQuestion("Are you sure you want to register this trainer (y/n empty=n): ", true, false)) {
                if (TrainerDAO.registerTrainer(t)) {
                    System.out.println("Registered trainer successfully!");
                } else {
                    System.out.println("Failed to register trainer!");
                }
            }
        } while (askUserYesNoQuestion("register next trainer (y/n empty=n): ", true, false));
    }

    private void registerNewPokemonType() {
        System.out.println();
        printTitle("Registering new pokemon type:");
        do {
            String type = askUserString("first name: ", 2);
            PokemonType pt = new PokemonType(type);
            System.out.println(pt);
            if (askUserYesNoQuestion("Are you sure you want to register this pokemon type (y/n empty=n): ", true, false)) {
                if (PokemonTypeDAO.registerPokemonType(pt)) {
                    System.out.println("Registered pokemon type successfully!");
                } else {
                    System.out.println("Failed to register pokemon type!");
                }
            }
        } while (askUserYesNoQuestion("register next pokemon type (y/n empty=n): ", true, false));
    }

    private void registerNewPokemonSpecies(){
        System.out.println();
        printTitle("Registering new pokemon specie:");
        //TODO: registerNewPokemonSpecies
        System.out.println("TODO");
    }

    private void registerNewPokemonPets(){
        System.out.println();
        printTitle("Registering new pokemon pet:");
        //TODO: registerNewPokemonPets
        System.out.println("TODO");
//        do {
//            String name = askUserString("pet name(can be empty): ");
//            if(name.isBlank()) name = null;
//            int speciesId = askUserInteger( "SpeciesId: ",1);
//            int trainerId = askUserInteger( "TrainerId: ",1);
//            PokemonPet pp = new PokemonPet(name, speciesId, trainerId);
//            System.out.println(pp);
//            if (askUserYesNoQuestion("Are you sure you want to register this pokemon pet (y/n empty=n): ", true, false)) {
//                if (PokemonPetDAO.registerPokemonPet(pp)) {
//                    System.out.println("Registered pokemon pet successfully!");
//                } else {
//                    System.out.println("Failed to register pokemon pet!");
//                }
//            }
//        } while (askUserYesNoQuestion("register next pokemon pet (y/n empty=n): ", true, false));

    }

    private void detailsTrainerMenu() {
        List<Trainer> list = TrainerDAO.getAllTrainers();
        if (list == null || list.size() == 0) {
            printAlert("No trainers in the database");
        } else {
            printTitle(String.format("%3s | %12s | %12s | %10s | %12s",
                    "id", "firstname", "lastname", "birthday", "city"));
            for (Trainer t : list) {
                System.out.printf("%3d | %12s | %12s | %10s | %12s\n",
                        t.getId(), t.getFirstName(), t.getLastName(), t.getBirthday(), t.getCity());
            }
            int trainerID = askUserInteger("Select trainerId: ");
            Trainer trainer = null;
            for (Trainer t : list)
                if (t.getId() == trainerID) {
                    trainer = t;
                    break;
                }
            if (trainer == null) {
                System.out.println("Invalid trainer id!");
            } else {
                printTrainerDetails(trainer);
                int choice;
                do {
                    System.out.println();
                    printTitle("Details trainer Menu: " + trainerID + ". "
                            + trainer.getFirstName() + " " + trainer.getLastName());
                    System.out.println("1. show trainer data");
                    System.out.println("2. remove trainer");
                    System.out.println("0. back to trainer menu");
                    choice = askUserInteger("Your choice: ", 0, 2);
                    switch (choice) {
                        case 1:
                            printTrainerDetails(trainer);
                            break;
                        case 2:
                            if (deleteTrainer(trainer))
                                return;
                            break;
                    }
                } while (choice != 0);
            }
        }
    }

    private void detailsPokemonTypeMenu() {
        List<PokemonType> list = PokemonTypeDAO.getAllPokemonTypes();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon types in the database");
        } else {
            printTitle(String.format("%3s | %12s", "id", "type"));
            for (PokemonType pt : list) {
                System.out.printf("%3d | %12s\n", pt.getId(), pt.getType());
            }
            int pokemonTypeId = askUserInteger("Select pokemon typeId: ");
            PokemonType pokemonType = null;
            for (PokemonType pt : list)
                if (pt.getId() == pokemonTypeId) {
                    pokemonType = pt;
                    break;
                }
            if (pokemonType == null) {
                System.out.println("Invalid pokemon type id!");
            } else {
                printPokemonTypeDetails(pokemonType);
                int choice;
                do {
                    System.out.println();
                    printTitle("Details pokemon type Menu: " + pokemonTypeId + ". " + pokemonType.getType());
                    System.out.println("1. show pokemon type data");
                    System.out.println("2. remove pokemon type");
                    System.out.println("0. back to pokemon type menu");
                    choice = askUserInteger("Your choice: ", 0, 2);
                    switch (choice) {
                        case 1:
                            printPokemonTypeDetails(pokemonType);
                            break;
                        case 2:
                            if (deletePokemonType(pokemonType))
                                return;
                            break;
                    }
                } while (choice != 0);
            }
        }
    }

    private void detailsPokemonSpeciesMenu() {
        List<PokemonSpecies> list = PokemonSpeciesDAO.getAllPokemonSpecies();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon types in the database");
        } else {
            printTitle(String.format("%3s | %12s", "id", "name"));
            for (PokemonSpecies ps : list) {
                System.out.printf("%3d | %12s\n", ps.getId(), ps.getName());
            }
            int pokemonTypeId = askUserInteger("Select pokemon typeId: ");
            PokemonSpecies pokemonSpecies = null;
            for (PokemonSpecies ps : list)
                if (ps.getId() == pokemonTypeId) {
                    pokemonSpecies = ps;
                    break;
                }
            if (pokemonSpecies == null) {
                System.out.println("Invalid pokemon type id!");
            } else {
                printPokemonSpeciesDetails(pokemonSpecies);
                int choice;
                do {
                    System.out.println();
                    printTitle("Details pokemon type Menu: " + pokemonTypeId + ". " + pokemonSpecies.getName());
                    System.out.println("1. show pokemon type data");
                    System.out.println("2. remove pokemon type");
                    System.out.println("0. back to pokemon type menu");
                    choice = askUserInteger("Your choice: ", 0, 2);
                    switch (choice) {
                        case 1:
                            printPokemonSpeciesDetails(pokemonSpecies);
                            break;
                        case 2:
                            if (deletePokemonSpecies(pokemonSpecies))
                                return;
                            break;
                    }
                } while (choice != 0);
            }
        }
    }

    private void detailsPokemonPetMenu() {
        List<PokemonPet> list = PokemonPetDAO.getAllPokemonPets();
        if (list == null || list.size() == 0) {
            printAlert("No pokemon types in the database");
        } else {
            printTitle(String.format("%3s | %12s", "id", "name"));
            for (PokemonPet pp : list) {
                System.out.printf("%3d | %12s\n", pp.getId(), pp.getName());
            }
            int pokemonTypeId = askUserInteger("Select pokemon typeId: ");
            PokemonPet pokemonPet = null;
            for (PokemonPet pp : list)
                if (pp.getId() == pokemonTypeId) {
                    pokemonPet = pp;
                    break;
                }
            if (pokemonPet == null) {
                System.out.println("Invalid pokemon type id!");
            } else {
                printPokemonPetDetails(pokemonPet);
                int choice;
                do {
                    System.out.println();
                    printTitle("Details pokemon type Menu: " + pokemonTypeId + ". " + pokemonPet.getName());
                    System.out.println("1. show pokemon type data");
                    System.out.println("2. remove pokemon type");
                    System.out.println("0. back to pokemon type menu");
                    choice = askUserInteger("Your choice: ", 0, 2);
                    switch (choice) {
                        case 1:
                            printPokemonPetDetails(pokemonPet);
                            break;
                        case 2:
                            if (deletePokemonPet(pokemonPet))
                                return;
                            break;
                    }
                } while (choice != 0);
            }
        }
    }

    void printTrainerDetails(Trainer t) {
        System.out.println();
        System.out.println("ID: " + t.getId());
        System.out.println("First name: " + t.getFirstName());
        System.out.println("Last name: " + t.getLastName());
        System.out.println("Birthday: " + t.getBirthday());
        System.out.println("City: " + t.getCity());
        System.out.println("Pokemon owned: ");
        if (askUserYesNoQuestion("Show all owned pokemon? (y/n empty=n): ", true, false)) {
            // TODO: show all pokemon owned by this trainer
        }
        askPressEnterToContinue();
    }

    void printPokemonTypeDetails(PokemonType pt) {
        System.out.println();
        System.out.println("ID: " + pt.getId());
        System.out.println("Type: " + pt.getType());
        System.out.println("Pokemon species: ");
        if (askUserYesNoQuestion("Show all pokemon of this type? (y/n empty=n): ", true, false)) {
            // TODO: show all pokemon of this type
        }
        askPressEnterToContinue();
    }

    void printPokemonSpeciesDetails(PokemonSpecies ps) {
        System.out.println();
        System.out.println("ID: " + ps.getId());
        System.out.println("Name: " + ps.getName());
        System.out.println("Type1: " + ps.getTypeId1() + " = " + PokemonTypeDAO.getTypeStrById(ps.getTypeId1()));
        System.out.println("Type2: " + ps.getTypeId2() + " = " + PokemonTypeDAO.getTypeStrById(ps.getTypeId2()));
        System.out.println("Parent evolution: " + ps.getParentEvolutionId());
        System.out.println("Official Id: " + ps.getParentEvolutionId());
        askPressEnterToContinue();
    }

    void printPokemonPetDetails(PokemonPet pp) {
        System.out.println();
        System.out.println("ID: " + pp.getId());
        System.out.println("Name: " + pp.getName());
        System.out.println("Species: " + pp.getSpeciesID() + PokemonSpeciesDAO.getSpeciesNameById(pp.getId()));
        System.out.println("Trainer: " + pp.getTrainerID() + TrainerDAO.getTrainerNameById(pp.getId()));
        askPressEnterToContinue();
    }

    private boolean deleteTrainer(Trainer t) {
        boolean deleted = false;
        if (t != null && t.getId() > 0) {
            int trainerID = t.getId();
            if (askUserYesNoQuestion("Are you sure you want to delete trainer " + trainerID + "?: ")) {
                if (TrainerDAO.removeTrainerById(trainerID)) {
                    deleted = true;
                    System.out.println("Removed trainer successfully!");
                } else
                    System.out.println("Failed to remove trainer!");
            } else {
                System.out.println("Remove canceled!");
            }
        } else {
            System.out.println("Invalid trainer id!");
        }

        askPressEnterToContinue();
        return deleted;
    }

    private boolean deletePokemonType(PokemonType pt) {
        boolean deleted = false;
        if (pt != null && pt.getId() > 0) {
            int typeId = pt.getId();
            if (askUserYesNoQuestion("Are you sure you want to delete pokemon type " + typeId + "?: ")) {
                if (PokemonTypeDAO.removePokemonTypeById(typeId)) {
                    deleted = true;
                    System.out.println("Removed pokemon type successfully!");
                } else
                    System.out.println("Failed to remove pokemon type!");
            } else {
                System.out.println("Remove canceled!");
            }
        } else {
            System.out.println("Invalid pokemon type id!");
        }

        askPressEnterToContinue();
        return deleted;
    }

    private boolean deletePokemonSpecies(PokemonSpecies ps) {
        boolean deleted = false;
        if (ps != null && ps.getId() > 0) {
            int speciesId = ps.getId();
            if (askUserYesNoQuestion("Are you sure you want to delete pokemon specie " + speciesId + "?: ")) {
                if (PokemonSpeciesDAO.removePokemonSpeciesById(speciesId)) {
                    deleted = true;
                    System.out.println("Removed pokemon specie successfully!");
                } else
                    System.out.println("Failed to remove pokemon specie!");
            } else {
                System.out.println("Remove canceled!");
            }
        } else {
            System.out.println("Invalid pokemon specie id!");
        }

        askPressEnterToContinue();
        return deleted;
    }

    private boolean deletePokemonPet(PokemonPet pp) {
        boolean deleted = false;
        if (pp != null && pp.getId() > 0) {
            int petId = pp.getId();
            if (askUserYesNoQuestion("Are you sure you want to delete pokemon pet " + petId + "?: ")) {
                if (PokemonPetDAO.removePokemonPetById(petId)) {
                    deleted = true;
                    System.out.println("Removed pokemon pet successfully!");
                } else
                    System.out.println("Failed to remove pokemon pet!");
            } else {
                System.out.println("Remove canceled!");
            }
        } else {
            System.out.println("Invalid pokemon pet id!");
        }

        askPressEnterToContinue();
        return deleted;
    }
}
