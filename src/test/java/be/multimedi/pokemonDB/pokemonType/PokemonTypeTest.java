package be.multimedi.pokemonDB.pokemonType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PokemonTypeTest {
    @Test
    void createPokemonTypeTest(){
        PokemonType pt = new PokemonType(100, "Lazy");
        assertEquals(100, pt.getId());
        assertEquals("Lazy", pt.getType());

        assertThrows( IllegalArgumentException.class, ()-> pt.setType(null));
        assertThrows( IllegalArgumentException.class, ()-> pt.setType(" "));
    }

}