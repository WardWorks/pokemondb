package be.multimedi.pokemonDB.pokemonType;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PokemonTypeDAOTest {

    @Test
    void getAllPokemonTypesTest() {
        List<PokemonType> list = PokemonTypeDAO.getAllPokemonTypes();
        assertTrue( list.size() > 1 );
    }

    @Test
    void registerGetByTypeNameAndRemovePokemonTypeTest() {
        // TODO registerGetByTypeNameAndRemovePokemonTypeTest
    }
}