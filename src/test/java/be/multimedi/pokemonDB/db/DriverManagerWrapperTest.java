package be.multimedi.pokemonDB.db;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static be.multimedi.pokemonDB.db.DriverManagerWrapper.*;
import static org.junit.jupiter.api.Assertions.*;

class DriverManagerWrapperTest {
    @Test
    void DatabaseConnectionTest(){
        try(Connection con = DriverManager.getConnection(url, login, pwd)){
            assertFalse( con.isClosed() );
        }catch (SQLException se){
            System.out.println(se);
            fail();
        }
    }
}