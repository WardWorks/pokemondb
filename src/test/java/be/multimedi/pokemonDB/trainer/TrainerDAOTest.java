package be.multimedi.pokemonDB.trainer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrainerDAOTest {
   @Test
   public void getTrainerByIdTest(){
      Trainer t = TrainerDAO.getTrainerById(1);
      assertNotNull(t);
      assertEquals(1, t.getId());
      assertEquals( "Ash", t.getFirstName());
      assertEquals( "Ketchum", t.getLastName());
      assertEquals( LocalDate.of(1985, 01, 01), t.getBirthday());
      assertEquals( "Pallet town", t.getCity());
   }

   @Test
   public void getAllTrainers(){
      List<Trainer> list = TrainerDAO.getAllTrainers();
      assertTrue(list.size() > 1);
   }

   @Test
   public void registerGetByFirstNameAndRemoveTrainerTest(){
      assertTrue(TrainerDAO.registerTrainer(
              new Trainer("Ward", "Truyen",
                      LocalDate.of(1984, 9, 3), "Boutersem")));
      List<Trainer> list = TrainerDAO.getTrainersByFirstName("Ward");
      Trainer t = list.get(0);
      assertTrue( TrainerDAO.removeTrainer( t ) );
   }

}